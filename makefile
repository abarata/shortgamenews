backup:
	mysqldump -u root -p --add-drop-database --add-drop-table --add-drop-trigger shortgamenews > shortgamenewsDROP.sql
	mysqldump -u root -p shortgamenews > shortgamenews.sql

restoreDROP:
	mysql -u root -p shortgamenews < shortgamenews.sql

restore:
	mysql -u root -p shortgamenews < shortgamenewsDROP.sql

installMysql:
	sudo apt-get install mysql-server
	sudo mysql_secure_installation
	sudo apt-get install libmysqlclient-dev

installPackages: npm bower

npm:
	sudo npm install -g gulp
	sudo npm install -g browserify
	sudo npm install -g bower
	npm install

#Si no funciona, instala de forma manual
node:
	wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash
	nvm install node
	nvm use node

bower:
	bower install uikit

# tener instalado python-dev y build-essential. Ambos son con apt-get install
# Tambien instalar python3.5-dev o la version que sea
run: env
	.env/bin/pip install django
	.env/bin/pip install sorl-thumbnail
	.env/bin/pip install markdown
	.env/bin/pip install pillow
	.env/bin/pip install gunicorn
	.env/bin/pip install mysqlclient

pip3:
	sudo apt-get install -y python3-pip

installEnv: pip3
	pip3 install virtualenv;

installEnvApt:
	@type virtualenv >/dev/null 2>&1 || sudo apt-get install virtualenv

# export LC_ALL=C
env: installEnvApt
	virtualenv -p python3 .env

test:
	@type virtualenv >/dev/null 2>&1 || echo 'Este programa no existe'

update:
	git fetch origin master
	git pull origin master
	.env/bin/python3.5 ./manage.py migrate
	.env/bin/python3.5 ./manage.py collectstatic
