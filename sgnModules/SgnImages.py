from urllib.parse import unquote
import os
from datetime import datetime
from django.conf import settings

def changeImagePath(instance,filename):
    date = datetime.now().strftime("%d-%m-%Y")
    if instance.id:
        oldUrl = instance.image.url
        date = instance.date.strftime("%d-%m-%Y")


    if(instance.game):
        if(instance.platform):
            newUrl = "photo/{0}/{1}/{2}/{3}".format(
                instance.game.name.replace(' ','_').lower(),
                instance.platform.name.replace(' ','_').lower(),
                date,
                filename)
        else:
            newUrl = "photo/{0}/{1}/{2}".format(
                instance.game.name.replace(' ','_').lower(),
                date,
                filename)
    else:
        if(instance.platform):
            newUrl = "photo/{0}/{1}/{2}".format(
                instance.platform.name.replace(' ','_').lower(),
                date,
                filename)
        else:
            newUrl = "photo/{0}/{1}/{2}".format(
                'noPlatform',
                date,
                filename)

    if instance.id:
        instance.image = newUrl
        # crea los directorios

        try:
            os.makedirs(settings.BASE_DIR +'/..' + os.path.split(unquote(instance.image.url))[0])
        except FileExistsError:
            pass

        # cambia el fichero de sitio
        #try:
        os.rename(settings.BASE_DIR +'/..' + unquote(oldUrl),settings.BASE_DIR +'/..' + unquote(instance.image.url))
        #except FileNotFoundError:
        #    pass
    else:
        return newUrl
