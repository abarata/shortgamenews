import json
from django.db.models.fields import Field, AutoField
from django.db import models
from django.forms.models import model_to_dict

def autocomplete(name,query):
    ac = []
    for data in query:
        ac.append({'value': data[name]})
    return json.dumps(ac)

def querySetToJson(query):
    jsonQuery = []
    print(query)
    for data in query:
        temp = {}
        for vb in data._meta.get_fields():
            if isinstance(vb,models.ManyToManyField):
                if getattr(data,vb.name).exists():
                    temp[vb.name] = list(getattr(data,vb.name).all().values())

            elif isinstance(vb,models.ForeignKey):
                print('ES UN FOREINGKEY')
                if getattr(data,vb.name):
                    print(model_to_dict(getattr(data,vb.name)))
                    temp[vb.name] = model_to_dict(getattr(data,vb.name))

            else:
                temp[vb.name] = getattr(data,vb.name)
        jsonQuery.append(temp)

    #print(jsonQuery)
    return jsonQuery
