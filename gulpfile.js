'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserify = require("browserify");
var babelify = require("babelify");
var fs = require("fs");
var concat = require('gulp-concat');
var uglifyify = require('uglifyify');
var UglifyJS = require('uglify-js');

gulp.task('sass', function () {
 return gulp.src('./news/static/news/css/shortGameNews.sass')
   .pipe(sass({outputStyle: 'compressed',includePaths: './bower_components/uikit/scss'}).on('error', sass.logError))
   .pipe(gulp.dest('./news/static/news/css'));
});

gulp.task('js',function(){
  // Para produccion
  process.env.NODE_ENV = 'production';
  return browserify({ debug: true })
  .transform(babelify)
  //.transform(uglifyify,{global: true})
  .require('./news/static/news/js/sgn.js', { entry: true })
  .bundle()
  .on("error", function (err) { console.log("Error: " + err.message); this.emit('end');})
  .pipe(fs.createWriteStream("./news/static/news/js/bundle.min.js"))
});

gulp.task('uglify',['js'],function(){
  return fs.writeFile('./news/static/news/js/bundle.min.js',UglifyJS.minify("./news/static/news/js/bundle.min.js").code)
});

gulp.task('concatUIkit',function(){
  return gulp.src(['./news/static/news/js/uikit.min.js','./news/static/news/js/components/*.js'])
  .pipe(concat('uikitSgn.js'))
  .pipe(gulp.dest('./news/static/news/js/'))
})

gulp.task('watch', function(){
  gulp.watch('./news/static/news/css/*.sass', ['sass']);
  gulp.watch(['./news/static/news/js/sgn.js','./news/static/news/js/reactComponents/*.js'],['js'])
});

gulp.task('production',function(){

});
