from django import template
import markdown
from django.utils.html import escape, strip_tags, mark_safe

register = template.Library()

@register.filter(is_safe=False)
def mk(value):
    return mark_safe(markdown.markdown(strip_tags(value)))
    #return escape("<div>Probando</div>")

@register.inclusion_tag('shareButtons.html')
def socialButtons(**kwargs):
    return {'options': kwargs}
