from django.db import models
import os
from datetime import datetime
from django.core.exceptions import *
from sorl.thumbnail import ImageField
from sgnModules.SgnImages import changeImagePath

class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name + '(Instance)'

class Platform(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Game(models.Model):
    name = models.CharField(max_length = 255,unique=True)
    web = models.URLField(blank=True)
    platform = models.ManyToManyField(Platform,blank=True)

    def __str__(self):
        return self.name

def changePath(instance,filename):
    if(instance.game):
        if(instance.platform):
            return "photo/{0}/{1}/{2}/{3}".format(
                instance.game.name.replace(' ','_').lower(),
                instance.platform.name.replace(' ','_').lower(),
                datetime.now().strftime("%d-%m-%Y"),
                filename)
        else:
            return "photo/{0}/{1}/{2}".format(
                instance.game.name.replace(' ','_').lower(),
                datetime.now().strftime("%d-%m-%Y"),
                filename)


    if(instance.platform):
        return "photo/{0}/{1}/{2}".format(
            instance.platform.name.replace(' ','_').lower(),
            datetime.now().strftime("%d-%m-%Y"),
            filename)

    return "photo/{0}/{1}/{2}".format(
        'noPlatform',
        datetime.now().strftime("%d-%m-%Y"),
        filename)

class Picture(models.Model):
    name = models.CharField(max_length=255)
    image = ImageField(upload_to=changeImagePath)
    game = models.ForeignKey(Game,blank=True,null=True)
    platform = models.ForeignKey(Platform,blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)


    def save(self, *args, **kwargs):
        if self.id:
            kwargs['update_fields'] = None

        super(Picture, self).save(*args, **kwargs) # Call the "real" save() method.


    def __str__(self):
        return self.name


class News(models.Model):
    title = models.CharField(max_length = 255)
    text = models.TextField()
    nameSource = models.CharField(max_length = 255,blank=True)
    source = models.URLField(blank=True)
    date = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    published = models.BooleanField(default=False)
    category = models.ForeignKey(Category,blank=True,null=True)
    games = models.ManyToManyField(Game,blank=True)
    platforms = models.ManyToManyField(Platform,blank=True)
    images = models.ManyToManyField(Picture,blank=True)

    class Meta:
        ordering = ['-date']
        permissions = (("can_preview", "Can preview a news"),)

    def __str__(self):
        return self.title
