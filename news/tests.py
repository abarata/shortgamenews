from django.test import TestCase
from news.models import *

class GameTestCase(TestCase):
    def setUp(self):
        Game.objects.create(name='Playstation')
        Category.objects.create(name='review')

    def testGameModel(self):
        # getting a instance of Game model
        playstation = Game.objects.get(name='Playstation')
        # Checking instance
        self.assertIsInstance(playstation,Game)
        # checking parameter
        self.assertEqual(playstation.name,'Playstation')
        # change name
        playstation.name = 'xbox'
        playstation.save()
        # chekind that the name is changed
        self.assertEqual(playstation.name,'xbox')
        # deleting the game
        playstation.delete()
        # checking the game is dropped from the database
        self.assertRaises(Game.DoesNotExist,Game.objects.get,name='xbox')

    def testCategoryModel(self):
        # getting instance of Category model
        category = Category.objects.get(name='review')
        # Checking instance
        self.assertIsInstance(category,Category)
        # Checking parameter
        self.assertEqual(category.name,'review')
        # change name
        category.name = 'prereview'
        category.save()
        # checking that the name is changed
        self.assertEqual(category.name,'prereview')
        # deleting the game
        category.delete()
        #checking the game is dropped from the database
        self.assertRaises(Category.DoesNotExist,Category.objects.get,name='prereview')
