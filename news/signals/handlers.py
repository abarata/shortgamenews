from django.db.models.signals import post_delete, pre_save, post_save
from django.dispatch import receiver
from news.models import Picture, Game, Platform
from django.conf import settings
import os
import shutil
from urllib.parse import unquote
from sgnModules.SgnImages import changeImagePath


@receiver(post_delete,sender=Picture)
def deleteImage(sender,**kwargs):
    if os.path.exists(settings.BASE_DIR + unquote(kwargs['instance'].image.url)):
        os.remove(settings.BASE_DIR + unquote(kwargs['instance'].image.url))

@receiver(pre_save,sender=Picture)
def changeLocationImage(sender,**kwargs):
    if(kwargs['instance'].id):
        #old = Picture.objects.get(pk=kwargs['instance'].id)

        changeImagePath(kwargs['instance'],os.path.basename(kwargs['instance'].image.name))

        # Elimino el antiguo
        #os.remove(settings.BASE_DIR + unquote(old.image.url))

@receiver(post_save,sender=Game)
def gameChanged(sender,**kwargs):
    pictures = kwargs['instance'].picture_set.all()
    for image in pictures:
        image.save()

@receiver(post_save,sender=Platform)
def platformChanged(sender,**kwargs):
    pictures = kwargs['instance'].picture_set.all()
    for image in pictures:
        image.save()
