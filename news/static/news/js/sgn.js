var SgnGallery = require('./reactComponents/sgnGallery');
var SgnSearchNews = require('./reactComponents/sgnSearchNews')
var reactDom = require('react-dom');

var loadImages = function(){
  $('button[data-sgn-images]').map(
    function(index,element){
      $(element).on(
        'click',
        function(){
          //console.log('Va a hacer el ajax')
          var id = this.id;
          $.ajax({
            url: '/loadImages/' + this.id,
            success: function(data,status){
              //console.log(id)
              //console.log(document.getElementsByClassName('gallery-' + id))
              reactDom.render(
                <SgnGallery pictures={data.pictures} />,
                document.getElementsByClassName('gallery-' + id)[0]
              );
            },
            error: function(request,status){
              console.log(request);
            }
          });
        }
      )
    }
  );
}

var formSearchNews = function(){
  $('.sgn-form-searchNews').submit(function(event){
    event.preventDefault();
    $.ajax({
      data: $( this ).serialize(),
      dataType: 'json',
      method: 'POST',
      success: function(data){
        //console.log('Los datos se han enviado');
        //console.log(data);
        reactDom.render(
          <SgnSearchNews news={data.status} />,
          document.getElementById('sgn-newsSearch-articles')
        )
      }
    });
    //console.log($( this ).serialize())
  });
}

sgn = (function(){
  var init = function(){
    loadImages();
    formSearchNews();
  }
  return {
    init: init,
    loadImages: loadImages,
    formSearchNews: formSearchNews
  }
})();

$(function(){
  sgn.init()
})
