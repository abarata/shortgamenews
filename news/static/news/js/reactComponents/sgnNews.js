React = require('react')

var Gallery = React.createClass({

  render: function(){
    var handler = this.handler;
    elements = this.props.pictures.map(function(element,index){
      return (
        <div className="uk-thumbnail uk-thumbnail-medium" key={element.id}>
          <a href='#' onClick={handler}><img id={"image-" + index} src={element.small} alt={element.name} /></a>
          <div className="uk-thumbnail-caption">{element.name}</div>
        </div>
      )
    });

    return(
      <div>
        {elements}
      </div>
    )
  }
