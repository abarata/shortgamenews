React = require('react');

var SearchNews = React.createClass({

  componentDidMount: function(){
    sgn.loadImages();
  },

  render: function(){

    var handler = this.handler;
    elements = this.props.news.map(function(element,index){
      var category;
      var platforms;
      var games;
      var source;
      var pictures;
      if(element.hasOwnProperty('category')){
        category = <span className="uk-badge">{ element.category.name }</span>
      }
      if (element.hasOwnProperty('platforms')){

        platforms = element.platforms.map(function(platform,indexPlatform){
          return <span key={'platforms-' + platform.id } className="uk-badge uk-badge-success">{ platform.name }</span>
        });
      }

      if(element.hasOwnProperty('games')){

        temp = element.games.map(function(game,indexGames){
          return <li key={ 'games-' + game.id }><a href={ game.web } rel="nofollow" target="_blank">{game.name}</a></li>
        });

        games = (
          <ul className="uk-nav uk-nav-side">
            <li className='uk-nav-header'>More about</li>
            {temp}
          </ul>
        );
      }

      if(element.hasOwnProperty('images')){

        pictures = (
          <div className={ 'gallery-' + element.id }>
            <button id={ element.id } className='uk-button uk-button-primary' data-sgn-images>Load images</button>
          </div>
        )

      }
      if(element.hasOwnProperty('source')){
        source = <span className='uk-text-bold'>Source: <a href={ element.source } target="_blank">{ element.nameSource }</a></span>
      }
      return (
        <article className="uk-article" key={'article-' + element.id}>
          <h1 className="uk-article-title">{ element.title }</h1>
          <div className='sgn-article-padding'>
            <div className='sgn-article-meta'>
              { category }
              { platforms }
              <p className="uk-article-meta">
                Date: { element.updated }
              </p>
            </div>
            <hr className="uk-article-divider" />

            <div className='sgn-article-text'>
              <div className='uk-grid'>
                <div className='uk-width-medium-7-10'>
                  <span  dangerouslySetInnerHTML={{__html: element.text}}></span>
                  { pictures }
                </div>
                <div className='uk-width-medium-3-10'>
                  { games }
                </div>
                <div className='uk-width-1-1 sgn-source uk-margin-top'>
                  { source }
                </div>
              </div>
            </div>
          </div>
        </article>
      )
    });

    return(
      <div>
        {elements}
      </div>
    )
  }

});

module.exports = SearchNews;
