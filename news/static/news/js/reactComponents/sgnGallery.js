React = require('react')

var Gallery = React.createClass({

  getInitialState: function() {
    //console.log(this.props.pictures)
    return {pictures: this.props.pictures}
  },

  handler: function(event){
    event.preventDefault();
    //console.log(event.target.id.split('-')[1]);
    UIkit.lightbox.create([{'source': this.state.pictures[event.target.id.split('-')[1]].large}]).show();
  },

  render: function(){
    var handler = this.handler;
    elements = this.props.pictures.map(function(element,index){
      return (
        <div className="uk-thumbnail uk-thumbnail-medium" key={element.id}>
          <a href='#' onClick={handler}><img id={"image-" + index} src={element.small} alt={element.name} /></a>
          <div className="uk-thumbnail-caption">{element.name}</div>
        </div>
      )
    });

    return(
      <div>
        {elements}
      </div>
    )
  }
});


module.exports = Gallery;
