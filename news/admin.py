from django.contrib import admin
from .models import Game, Picture, News, Category, Platform
from django.shortcuts import redirect
from django.urls import reverse
from sorl.thumbnail.admin import AdminImageMixin
from django.core import serializers

@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    filter_horizontal = ['platform']
    list_display = ['name']

@admin.register(Picture)
class PictureAdmin(AdminImageMixin,admin.ModelAdmin):
    list_display = ['name','game','platform','date']

    actions = ['changePlatformGame']
    list_filter = ['name','game','platform','date']
    search_fields = ['name','game__name','platform__name','date']

    #def save_model(self, request, obj, form, change):
    #    if obj.image:
    #        super(PictureAdmin,self).save_model(request, obj, form, change)
    #    else:
    #        self.message_user(request,'ERROR FROM IMAGE WEEEEEEE',level=messages.ERROR)

    def changePlatformGame(self, request, queryset):
        pictures = []
        for qs in queryset:
            pictures.append(qs.id)

        return redirect('news:changePlatformGame',query=pictures)

    changePlatformGame.short_description = "Change platform or game"


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    filter_horizontal = ['games','images','platforms']
    list_display = ['title','date','updated','published' ,'pk']
    list_filter = ['games','platforms','published','date']
    search_fields = ['title','games__name','platforms__name','images__title','published']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass

@admin.register(Platform)
class PlatformAdmin(admin.ModelAdmin):
    pass


# Register your models here.
