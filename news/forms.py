from django import forms

class SearchNewsForms(forms.Form):
    #title = forms.CharField(label='Title news', max_length=255)
    games = forms.CharField(label='Game', max_length=255, required=False)
    platforms = forms.CharField(label='Platform', max_length=255, required=False)
    category = forms.CharField(label='Category', max_length=255, required=False)
    order = forms.ChoiceField(label='Order by date',choices=[['','older'],['-','newer']],required=False)
