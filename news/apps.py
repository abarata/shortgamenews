from django.apps import AppConfig

class NewsConfig(AppConfig):
    name = 'news'

    def ready(self):
        from .signals.handlers import deleteImage
        from .signals.handlers import changeLocationImage
        from .signals.handlers import gameChanged
        from .signals.handlers import platformChanged
