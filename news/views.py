from django.shortcuts import render
from django.views import View
from .models import News, Picture, Game, Platform, Category
from django.utils.html import escape, strip_tags, mark_safe
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import markdown
from sorl.thumbnail import get_thumbnail
from django.http import JsonResponse
from django.core import serializers
from django.contrib.auth.mixins import PermissionRequiredMixin
from .forms import SearchNewsForms
from sgnModules.SgnJson import autocomplete, querySetToJson
#from django.db.models import Q
#from django.db.models.query import QuerySet

class Index(View):
    templateName = 'news/show.html'
    def get(self,request,*args,**kwargs):
        news = News.objects.filter(published=True)[:30]
        paginator = Paginator(news,10)
        page = request.GET.get('page')
        try:
            news = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            news = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            news = paginator.page(paginator.num_pages)

        #for n in news:
        #    n.text = markdown.markdown(strip_tags(n.text))
        return render(request,self.templateName,{'news': news,'path': request.get_full_path()})

class ChangePlatformGame(View):
    def get(self,request,*args,**kwargs):
        test = serializers.deserialize('json',kwargs['query'])
        return JsonResponse({'da': test[0]})

    def post(self,request,*args,**kwargs):
        return JsonResponse({'da': 'weeee'})

class LoadImages(View):
    def get(self,request,*args,**kwargs):
        pictures = News.objects.get(pk=args[0]).images.all()
        finalPictures = []
        for pict in pictures:
            finalPictures.append({
                'small': get_thumbnail(pict.image, '640x480', quality=60).url,
                'large': pict.image.url,
                'name': pict.name,
                'id': pict.id
            })
        return JsonResponse({'pictures': finalPictures})

class Faq(View):
    templateName = 'news/faq.html'
    def get(self,request,*args,**kwargs):
        return render(request,self.templateName)

class Changelog(View):
    templateName = 'news/changelog.html'
    def get(self,request,*args,**kwargs):
        return render(request,self.templateName)

class PreviewNews(PermissionRequiredMixin, View):
    templateName = 'news/show.html'
    permission_required = 'news.can_preview'
    raise_exception = True
    def get(self,request,*args,**kwargs):
        preview = News.objects.filter(pk=args[0])
        #for n in preview:
        #    n.text = markdown.markdown(strip_tags(n.text))
        return render(request,self.templateName,{'news': preview})

class WatchOneNews(View):
    templateName = 'news/show.html'
    def get(self,request,*args,**kwargs):
        news = News.objects.filter(pk=args[0],published=True)
        #for n in news:
        #    n.text = markdown.markdown(strip_tags(n.text))
        return render(request,self.templateName,{'news': news})

class SearchNews(View):
    templateName = 'news/searchNews.html'
    def get(self,request,*args,**kwargs):
        #titleNews = News.objects.values('title')
        form = SearchNewsForms()
        dataJson = {
            'games': autocomplete('name',Game.objects.values('name')),
            'platforms': autocomplete('name',Platform.objects.values('name')),
            'category': autocomplete('name',Category.objects.values('name'))
        }
        return render(
            request,
            self.templateName,
            {
                'form': form,
                'dataJson': dataJson
            }
        )
    def post(self,request,*args,**kwargs):
        news = News.objects.filter(published = True)
        if request.POST['games']:
            news = news.filter(games__name = request.POST['games'])

        if request.POST['platforms']:
            news = news.filter(platforms__name = request.POST['platforms'])

        if request.POST['category']:
            news = news.filter(category__name = request.POST['category'])

        news = news.order_by(request.POST['order']+'date')

        # queryset to json
        news = querySetToJson(news)

        #markdown to html
        for value in news:
            value['text'] = markdown.markdown(strip_tags(value['text']))

        return JsonResponse({'status': news})

def learnMore():
    picture = Picture.objects.get(pk=20)
    #im = get_thumbnail(picture.image, '100x100', crop='center', quality=99)
    #return get_thumbnail(picture.image, '1280x720', quality=99)
    #return picture.image
