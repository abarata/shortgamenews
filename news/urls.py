from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^$',Index.as_view(), name='index'),
    url(r'^changePlatformGame/(?P<query>.+)/$',ChangePlatformGame.as_view(),name='changePlatformGame'),
    url(r'^faq/$',Faq.as_view(),name='faq'),
    url(r'^changelog/$',Changelog.as_view(),name='changelog'),
    url(r'^preview/(\d+)/$',PreviewNews.as_view(),name='preview'),
    url(r'^showone/(\d+)/$',WatchOneNews.as_view(),name='showOne'),
    url(r'^searchnews/$',SearchNews.as_view(),name='searchNews'),
    url(r'^loadImages/(\d+)/$',LoadImages.as_view(),name='loadImages')
]
